<?php
/**
 * File Name: Hilton_Client_A.php
 * Purpose: This page will be used as an interface to call the Hilton Services
 */

include_once("OTA_Curl_Services.php");

//For Ajax request t get the parameter for the specified service
if($_REQUEST['doAct']=='getParamArea'){
    $response = getParameterArea($_REQUEST['soapAction']);
    echo $response;
    return;
}//if

/**
* Name: [callHiltonService]
* Purpose: This method is used to call the specified Hilton transaction & print the response received.
* @param string $serviceName - Required - Service name
* @return void
*/

function callHiltonService($serviceName){

    if($serviceName==''){
        echo 'Please select a service';
        return;
    }
    
    //creating OTA Object
    $ht=new OTA_Curl_Services;

    switch($serviceName){
        case "OTA_Ping":
            $echoData = ($_POST['echo_data'])?$_POST['echo_data']:'';
            $res=$ht->OTA_Ping($echoData, $_POST['request_id']);            
            break;

        case "OTA_HotelDescriptiveInfo":
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']: '';
            $res=$ht->OTA_HotelDescriptiveInfo($hotelCode, $_POST['request_id']);
            break;

        case "OTA_Read":
            $resNo = ($_POST['res_no'])?$_POST['res_no']:'';
            $surName = ($_POST['sur_name'])?$_POST['sur_name']:'';
            $res=$ht->OTA_Read($resNo, $surName, $_POST['request_id']);
            break;

        case "OTA_HotelSearch":
            $latitude = ($_POST['latitude'])?$_POST['latitude']:'';
            $longitude = ($_POST['longitude'])?$_POST['longitude']:'';
            $address1 = ($_POST['address1'])?$_POST['address1']:'';
            $address2 = ($_POST['address2'])?$_POST['address2']:'';
            $address3 = ($_POST['address3'])?$_POST['address3']:'';
            $cityName = ($_POST['city_name'])?$_POST['city_name']:'';
            $postalCode = ($_POST['postal_code'])?$_POST['postal_code']:'';
            $stateCode = ($_POST['state_code'])?$_POST['state_code']:'';
            $countryCode = ($_POST['country_code'])?$_POST['country_code']:'';
            $locationCode = ($_POST['location_code'])?$_POST['location_code']:'';
            $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $distance = ($_POST['distance'])?$_POST['distance']:'';
            $distanceMeasure = ($_POST['distance_measure'])?$_POST['distance_measure']:'';

            $res=$ht->OTA_HotelSearch($latitude, $longitude, $address1, $address2,
                        $address3, $cityName, $postalCode, $stateCode, $countryCode,
                        $locationCode, $brandCode, $distance, $distanceMeasure, $_POST['request_id']);
            break;
        
        case "OTA_Cancel":
            $hiltonResNo = ($_POST['hilton_res_no'])?$_POST['hilton_res_no']:'';
            $clientConformationNo = ($_POST['client_confirmation_no'])?$_POST['client_confirmation_no']:'';
            $companyName = ($_POST['company_name'])?$_POST['company_name']:'';
            $personName = ($_POST['person_name'])?$_POST['person_name']:'';
            $surname = ($_POST['surname'])?$_POST['surname']:'';
            $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';
            $resStartDate = ($_POST['res_start_date'])?$_POST['res_start_date']:'';
            $resDuration = ($_POST['res_duration'])?$_POST['res_duration']:'';

            $res=$ht->OTA_Cancel($hiltonResNo, $clientConformationNo, $companyName, $personName,
                        $surname, $brandCode, $hotelCode, $resStartDate, $resDuration, $_POST['request_id'], $_POST['cancel_type']);
            break;

        case "OTA_HotelAvail":
            $arrivalDate = ($_POST['arrival_date'])?$_POST['arrival_date']:'';
            $noOfNights = ($_POST['num_nights'])?$_POST['num_nights']:'';
            $noOfRooms = ($_POST['num_rooms'])?$_POST['num_rooms']:'';
            $noOfAdults = ($_POST['num_adults'])?$_POST['num_adults']:'';
            $noOfChilds = ($_POST['num_childs'])?$_POST['num_childs']:'';
            $roomAmenity = ($_POST['room_amenity'])?$_POST['room_amenity']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';
            $promotionCode = ($_POST['promotion_code'])?$_POST['promotion_code']:'';
            $currencyCode = ($_POST['currency_code'])?$_POST['currency_code']:'';
            $ratePlanCategory1 = ($_POST['rate_plan_category1'])?$_POST['rate_plan_category1']:'';
            $ratePlanID1 = ($_POST['rate_plan_id1'])?$_POST['rate_plan_id1']:'';
            $ratePlanCategory2 = ($_POST['rate_plan_category2'])?$_POST['rate_plan_category2']:'';
            $ratePlanID2 = ($_POST['rate_plan_id2'])?$_POST['rate_plan_id2']:'';
            $ratePlanCategory3 = ($_POST['rate_plan_category3'])?$_POST['rate_plan_category3']:'';
            $ratePlanID3 = ($_POST['rate_plan_id3'])?$_POST['rate_plan_id3']:'';

            $res=$ht->OTA_HotelAvail($arrivalDate, $noOfNights, $currencyCode, $noOfRooms, $noOfAdults, $noOfChilds,
                    $roomAmenity, $hotelCode, $promotionCode, $ratePlanCategory1, $ratePlanID1, $ratePlanCategory2,
                     $ratePlanID2, $ratePlanCategory3, $ratePlanID3, $_POST['request_id']);
            break;
       
        case "OTA_HotelRes":
            $arrivalDate = ($_POST['arrival_date'])?$_POST['arrival_date']:'';
            $noOfNights = ($_POST['num_nights'])?$_POST['num_nights']:'';
            $noOfAdults = ($_POST['num_adults'])?$_POST['num_adults']:'';
            $noOfChilds = ($_POST['num_childs'])?$_POST['num_childs']:'';
            $promotionCode = ($_POST['promotion_code'])?$_POST['promotion_code']:'';
            $roomTypeCode = ($_POST['room_type_code'])?$_POST['room_type_code']:'';
            $roomAmenity = ($_POST['room_amenity'])?$_POST['room_amenity']:'';
            $bookingCode = ($_POST['booking_code'])?$_POST['booking_code']:'';
            $ratePlanID = ($_POST['rate_plan_id'])?$_POST['rate_plan_id']:'';
            $ratePlanCode = ($_POST['rate_plan_code'])?$_POST['rate_plan_code']:'';
	    $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';

	    $namePrefix = ($_POST['name_prefix'])?$_POST['name_prefix']:'';
	    $customerName = ($_POST['customer_name'])?$_POST['customer_name']:'';
	    $surname = ($_POST['surname'])?$_POST['surname']:'';
	    $telephone = ($_POST['telephone'])?$_POST['telephone']:'';
	    $email = ($_POST['email'])?$_POST['email']:'';
	    $address1 = ($_POST['address1'])?$_POST['address1']:'';
	    $address2 = ($_POST['address2'])?$_POST['address2']:'';
	    $address3 = ($_POST['address3'])?$_POST['address3']:'';
	    $cityName = ($_POST['city_name'])?$_POST['city_name']:'';
	    $postalCode = ($_POST['postal_code'])?$_POST['postal_code']:'';
	    $stateCode = ($_POST['state_code'])?$_POST['state_code']:'';
	    $countryCode = ($_POST['country_code'])?$_POST['country_code']:'';

            $customerDetails = array("NamePrefix"=>$namePrefix, "Name"=>$customerName, "Surname"=>$surname, "PhoneNumber"=>$telephone,
                     "Email"=>$email, "Address1"=>$address1, "Address2"=>$address2, "Address3"=>$address3,
                     "CityName"=>$cityName, "PostalCode"=>$postalCode, "StateCode"=>$stateCode, "CountryCode"=>$countryCode);

            //comments
            $comments = array();
	    if($_POST['comments1']) $comments[] = $_POST['comments1'];
	    if($_POST['comments2']) $comments[] = $_POST['comments2'];
            if($_POST['comments3']) $comments[] = $_POST['comments3'];

            //membership details
            $memAccID1 = ($_POST['mem_acc_id1'])?$_POST['mem_acc_id1']:'';
            $memProgramCode1 = ($_POST['mem_program_code1'])?$_POST['mem_program_code1']:'';
            $memAccID2 = ($_POST['mem_acc_id2'])?$_POST['mem_acc_id2']:'';
            $memProgramCode2 = ($_POST['mem_program_code2'])?$_POST['mem_program_code2']:'';
            $memberships = array(array("AccountID"=>$memAccID1,"ProgramCode"=>$memProgramCode1),array("AccountID"=>$memAccID2,"ProgramCode"=>$memProgramCode2));

            $guaranteeCode = ($_POST['guarantee_code'])?$_POST['guarantee_code']:'';
	    $cardCode = ($_POST['card_code'])?$_POST['card_code']:'';
	    $cardNumber = ($_POST['card_no'])?$_POST['card_no']:'';
	    $expireDate = ($_POST['expire_date'])?$_POST['expire_date']:'';
	    $resIDValue = ($_POST['res_id'])?$_POST['res_id']:'';
	    $resIDDate = ($_POST['res_date'])?$_POST['res_date']:'';
	    $resIDSource = ($_POST['res_source'])?$_POST['res_source']:'';
	    $resStatus = ($_POST['res_status'])?$_POST['res_status']:'';

            //calling OTA_HotelRes sevice
            $res=$ht->OTA_HotelRes($arrivalDate, $noOfNights, $noOfAdults, $noOfChilds, $customerDetails,
                    $promotionCode, $roomTypeCode, $roomAmenity, $bookingCode, $ratePlanID, $ratePlanCode,
                    $brandCode, $hotelCode, $memberships, $comments, $guaranteeCode, $cardCode, $cardNumber, $expireDate,
                    $resIDValue, $resIDDate, $resIDSource, $_POST['request_id'], $resStatus);
            break;
            
        case "OTA_HotelResModify":
            $arrivalDate = ($_POST['arrival_date'])?$_POST['arrival_date']:'';
            $noOfNights = ($_POST['num_nights'])?$_POST['num_nights']:'';
            $noOfAdults = ($_POST['num_adults'])?$_POST['num_adults']:'';
            $noOfChilds = ($_POST['num_childs'])?$_POST['num_childs']:'';
            $promotionCode = ($_POST['promotion_code'])?$_POST['promotion_code']:'';
            $roomTypeCode = ($_POST['room_type_code'])?$_POST['room_type_code']:'';
            $roomAmenity = ($_POST['room_amenity'])?$_POST['room_amenity']:'';
            $bookingCode = ($_POST['booking_code'])?$_POST['booking_code']:'';
            $ratePlanID = ($_POST['rate_plan_id'])?$_POST['rate_plan_id']:'';
            $ratePlanCode = ($_POST['rate_plan_code'])?$_POST['rate_plan_code']:'';
	    $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';

	    $namePrefix = ($_POST['name_prefix'])?$_POST['name_prefix']:'';
	    $customerName = ($_POST['customer_name'])?$_POST['customer_name']:'';
	    $surname = ($_POST['surname'])?$_POST['surname']:'';
	    $telephone = ($_POST['telephone'])?$_POST['telephone']:'';
	    $email = ($_POST['email'])?$_POST['email']:'';
	    $address1 = ($_POST['address1'])?$_POST['address1']:'';
	    $address2 = ($_POST['address2'])?$_POST['address2']:'';
	    $address3 = ($_POST['address3'])?$_POST['address3']:'';
	    $cityName = ($_POST['city_name'])?$_POST['city_name']:'';
	    $postalCode = ($_POST['postal_code'])?$_POST['postal_code']:'';
	    $stateCode = ($_POST['state_code'])?$_POST['state_code']:'';
	    $countryCode = ($_POST['country_code'])?$_POST['country_code']:'';

            $customerDetails = array("NamePrefix"=>$namePrefix, "Name"=>$customerName, "Surname"=>$surname, "PhoneNumber"=>$telephone,
                     "Email"=>$email, "Address1"=>$address1, "Address2"=>$address2, "Address3"=>$address3,
                     "CityName"=>$cityName, "PostalCode"=>$postalCode, "StateCode"=>$stateCode, "CountryCode"=>$countryCode);

            //comments
            $comments = array();
	    if($_POST['comments1']) $comments[] = $_POST['comments1'];
	    if($_POST['comments2']) $comments[] = $_POST['comments2'];
            if($_POST['comments3']) $comments[] = $_POST['comments3'];

            //membership details
            $memAccID1 = ($_POST['mem_acc_id1'])?$_POST['mem_acc_id1']:'';
            $memProgramCode1 = ($_POST['mem_program_code1'])?$_POST['mem_program_code1']:'';
            $memAccID2 = ($_POST['mem_acc_id2'])?$_POST['mem_acc_id2']:'';
            $memProgramCode2 = ($_POST['mem_program_code2'])?$_POST['mem_program_code2']:'';
            $memberships = array(array("AccountID"=>$memAccID1,"ProgramCode"=>$memProgramCode1),array("AccountID"=>$memAccID2,"ProgramCode"=>$memProgramCode2));

            $guaranteeCode = ($_POST['guarantee_code'])?$_POST['guarantee_code']:'';
	    $cardCode = ($_POST['card_code'])?$_POST['card_code']:'';
	    $cardNumber = ($_POST['card_no'])?$_POST['card_no']:'';
	    $expireDate = ($_POST['expire_date'])?$_POST['expire_date']:'';
	    $resIDValue = ($_POST['res_id'])?$_POST['res_id']:'';
	    $resIDDate = ($_POST['res_date'])?$_POST['res_date']:'';
	    $resIDSource = ($_POST['res_source'])?$_POST['res_source']:'';
            $localIdentifierValue = ($_POST['local_identifier_value'])?$_POST['local_identifier_value']:'';
            $resStatus = ($_POST['res_status'])?$_POST['res_status']:'';

            //calling OTA_HotelResModify sevice
            $res=$ht->OTA_HotelResModify($arrivalDate, $noOfNights, $noOfAdults, $noOfChilds, $customerDetails,
                    $promotionCode, $roomTypeCode, $roomAmenity, $bookingCode, $ratePlanID, $ratePlanCode,
                    $brandCode, $hotelCode, $memberships, $comments, $guaranteeCode, $cardCode, $cardNumber, $expireDate,
                    $resIDValue, $resIDDate, $resIDSource, $localIdentifierValue, $_POST['request_id'], $resStatus);
            break;

        default:
            $res=$ht->OTA_Ping('Testing HTE3');
            break;
    }//switch
    if($ht->error){
        echo "<br/><div class='error'>".$ht->error."</div>";
        return;
    }
    if($res) {//print the request xml, response xml 
        echo "<br/><br/>";
        //to print request xml
        echo '<h3>Request XML</h3>';
        echo "<textarea class='raw_resp'>".$ht->requestXML."</textarea>";
        echo "<br/><br/>";

        //to print response as a raw response
        echo '<h3>Response as a raw response</h3>';
        echo "<textarea class='raw_resp'>$res</textarea>";
        echo "<br/><br/><br/>";

        //to print response as an object
        //to make a simplexml object
        $res = str_replace('SOAP-ENV:', '', $res);
        //$res = str_replace('soap:', '', $res);
        $xmlobj = (array) simplexml_load_string($res);
        echo '<h3>Response as an object</h3>';
        echo "<div class='xml_resp'>";
        echo '<pre>';
        print_r($xmlobj['Body']);
        echo '</pre>';
        echo '</div>';
    }else {//to display the error
        echo "<br/><div class='error'>".$ht->error."</div>";
    }//else
}//function

/**
* Name: [getParameterArea]
* Purpose: This method is used to get the parameter area specified to the service.
* @param string $serviceName - Required - Service name
* @return void
*/

function getParameterArea($serviceName){

    $reqIdList = array();
    $requestIDDetails = '';
    $paramArea .= '<fieldset>
                    <legend>Parameters</legend></parameters>
                    <table class="param_area"><tbody>';

    switch($serviceName){

        case "OTA_Ping":
            //to set request id and request ID details
            $reqIdList = array(1);
            $requestIDDetails .= '<li> <b>1</b> : To check connectivity.</li>';

            //to display param fields
            $echoData = ($_REQUEST['echo_data'])?$_REQUEST['echo_data']:'';
            $paramArea.= '<tr><td class="label">Echo Data* : </td><td><input type="text" name="echo_data" id="echo_data" value="'.$echoData.'"/></td></tr>';
            break;

        case "OTA_Read":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103", "104");
            $requestIDDetails .= '
                <li> <b>101</b> : Unsuccessful request since the Last name in the re-quest does not match the last name in the reservation.</li>
                <li> <b>102</b> : Successful request for an existing reservation.</li>
                <li> <b>103</b> : Successful request for an existing reservation.</li>
                <li> <b>104</b> : Successful request for a cancelled reservation.</li>';

            //to display param fields
            $reservationNumber = ($_REQUEST['res_no'])?$_REQUEST['res_no']:'';
            $surName = ($_REQUEST['sur_name'])?$_REQUEST['sur_name']:'';

            $paramArea.= '<tr><td class="label">Booking Reference* : </td>
                <td><input type="text" name="res_no" id="res_no" value="'.$reservationNumber.'"/></td>';
            $paramArea.= '<td class="label">Last Name of Person* : </td>
                <td><input type="text" name="sur_name" id="sur_name" value="'.$surName.'"/></td></tr>';
            break;

        case "OTA_HotelDescriptiveInfo":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103");
            $requestIDDetails .= '
                <li> <b>101</b> : Unsuccessful request.  Hotel code must be all upper case.  Should receive an error message “Invalid hotel code”.</li>
                <li> <b>102</b> : Unsuccessful request.  Hotel code too long.  Should receive an error message “Invalid hotel code.”</li>
                <li> <b>103</b> : Successful request.</li>';

            //to display param fields
            $hotelCode = ($_REQUEST['hotel_code'])?$_REQUEST['hotel_code']: '';
            $paramArea.= '<tr><td class="label">Hotel Code* : </td><td><input type="text" name="hotel_code" id="hotel_code" value="'.$hotelCode.'"/></td></tr>';
            break;

        case "OTA_HotelSearch":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103", "104", "105", "106");
            $requestIDDetails .= '
                <li> <b>101</b> : Successful city search request which returns a list of properties.</li>
                <li> <b>102</b> : Unsuccessful reply due to insufficient or inaccurate ge-ographical information.</li>
                <li> <b>103</b> : Successful search request that uses a street address as the center of the search radius and returns a list of properties.</li>
                <li> <b>104</b> : Successful search request that uses a zip code as the center of a 5 mile radius and returns a list of properties.</li>
                <li> <b>105</b> : Successful search request that uses a three-letter air-port code and brand of HI as the center of the search radius and returns a list of properties.</li>
                <li> <b>106</b> : Successful search based on latitude and longitude.</li>';

            //to display param fields
            $latitude = ($_POST['latitude'])?$_POST['latitude']:'';
            $longitude = ($_POST['longitude'])?$_POST['longitude']:'';
            $address1 = ($_POST['address1'])?$_POST['address1']:'';
            $address2 = ($_POST['address2'])?$_POST['address2']:'';
            $address3 = ($_POST['address3'])?$_POST['address3']:'';
            $cityName = ($_POST['city_name'])?$_POST['city_name']:'';
            $postalCode = ($_POST['postal_code'])?$_POST['postal_code']:'';
            $stateCode = ($_POST['state_code'])?$_POST['state_code']:'';
            $countryCode = ($_POST['country_code'])?$_POST['country_code']:'';
            $locationCode = ($_POST['location_code'])?$_POST['location_code']:'';
            $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $distance = ($_POST['distance'])?$_POST['distance']:'';
            $distanceMeasure = ($_POST['distance_measure'])?$_POST['distance_measure']:'';

            $paramArea.= '<tr>
                    <td class="label">Latitude : </td>
                    <td><input type="text" name="latitude" id="latitude" value="'.$latitude.'"/></td>
                    <td class="label">Longitude : </td>
                    <td><input type="text" name="longitude" id="longitude" value="'.$longitude.'"/></td>
                    <td colspan="2"></td>
                   </tr>';
            $paramArea.= '<tr>
                    <td class="label">Address1 : </td>
                    <td><input type="text" name="address1" id="address1" value="'.$address1.'"/></td>
                    <td class="label">Address2 : </td>
                    <td><input type="text" name="address2" id="address2" value="'.$address2.'"/></td>
                    <td class="label">Address3 : </td>
                    <td><input type="text" name="address3" id="address3" value="'.$address3.'"/></td>
                   </tr>';
            $paramArea.= '<tr>
                    <td class="label">City Name : </td>
                    <td><input type="text" name="city_name" id="city_name" value="'.$cityName.'"/></td>
                    <td class="label">Postal Code : </td>
                    <td><input type="text" name="postal_code" id="postal_code" value="'.$postalCode.'"/></td>
                    <td class="label">State Code : </td>
                    <td><input type="text" name="state_code" id="state_code" value="'.$stateCode.'"/></td>
                   </tr>';
            $paramArea.= '<tr>
                    <td class="label">Country Code : </td>
                    <td><input type="text" name="country_code" id="country_code" value="'.$countryCode.'"/></td>
                    <td class="label">Location Code : </td>
                    <td><input type="text" name="location_code" id="location_code" value="'.$locationCode.'"/></td>
                    <td class="label">Brand Code : </td>
                    <td><input type="text" name="brand_code" id="brand_code" value="'.$brandCode.'"/></td>
                   </tr>';
            $kmSelected = ($distanceMeasure=='Km')?"selected":"";
            $mileSelected = ($distanceMeasure=='Mile')?"selected":"";
            $paramArea.= '<tr>
                    <td class="label">Distance : </td>
                    <td><input type="text" name="distance" id="distance" value="'.$distance.'"/></td>
                    <td class="label">Distance Measure : </td>
                    <td>
                        <select name="distance_measure" id="distance_measure" >
                            <option value="" >SELECT</option>
                            <option value="Mile"'.$mileSelected.' >Mile</option>
                            <option value="Km" '.$kmSelected.' >Km</option>
                        </select>
                    </td>
                    <td colspan="2"></td>
                   </tr>';
             break;

        case "OTA_Cancel":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103", "104", "105");
            $requestIDDetails .= '
                <li> <b>101</b> : Unsuccessful request since the Last name in the re-quest does not match the last name in the reservation.Successful Cancellation with CancelType=”Initiate”.</li>
                <li> <b>102</b> : Successful Cancellation with CancelType=”Initiate”.</li>
                <li> <b>103</b> : Successful Cancellation with CancelType=”Commit”.</li>
                <li> <b>104</b> : Unsuccessful request since the reservation is already cancelled.</li>
                <li> <b>105</b> : Unsuccessful request since the deadline for cancellation has expired.</li>';
            
            //to display param fields
            $hiltonResNo = ($_POST['hilton_res_no'])?$_POST['hilton_res_no']:'';
            $clientConformationNo = ($_POST['client_confirmation_no'])?$_POST['client_confirmation_no']:'';
            $companyName = ($_POST['company_name'])?$_POST['company_name']:'';
            $personName = ($_POST['person_name'])?$_POST['person_name']:'';
            $surname = ($_POST['surname'])?$_POST['surname']:'';
            $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';
            $resStartDate = ($_POST['res_start_date'])?$_POST['res_start_date']:'';
            $resDuration = ($_POST['res_duration'])?$_POST['res_duration']:'';
            $cancelType = ($_POST['cancel_type'])?$_POST['cancel_type']:'';

            $paramArea.= '<tr>
                    <td class="label">Hilton Reservation No* : </td>
                    <td><input type="text" name="hilton_res_no" id="hilton_res_no" value="'.$hiltonResNo.'"/></td>
                    <td class="label">Client Conformation No : </td>
                    <td><input type="text" name="client_confirmation_no" id="client_confirmation_no" value="'.$clientConformationNo.'"/></td>
                    <td class="label">Company Name : </td>
                    <td><input type="text" name="company_name" id="company_name" value="'.$companyName.'"/></td>
                   </tr>';

            $paramArea.= '<tr>
                    <td class="label">Person Name* : </td>
                    <td><input type="text" name="person_name" id="person_name" value="'.$personName.'"/></td>
                    <td class="label">Surname* : </td>
                    <td><input type="text" name="surname" id="surname" value="'.$surname.'"/></td>
                    <td class="label">Brand Code* : </td>
                    <td><input type="text" name="brand_code" id="brand_code" value="'.$brandCode.'"/></td>
                   </tr>';

            $paramArea.= '<tr>
                    <td class="label">Hotel Code* : </td>
                    <td><input type="text" name="hotel_code" id="hotel_code" value="'.$hotelCode.'"/></td>
                    <td class="label">Start Date* : </td>
                    <td><input type="text" name="res_start_date" id="res_start_date" value="'.$resStartDate.'"/></td>
                    <td class="label">Duration* : </td>
                    <td><input type="text" name="res_duration" id="res_duration" value="'.$resDuration.'"/></td>
                   </tr>';
            $paramArea.= '<tr>
                    <td class="label">Cancel Type : </td>
                    <td><input type="text" name="cancel_type"  id="cancel_type" readOnly= "true" value="'.$cancelType.'"/></td>
                    <td colspan="4"></td>';
            break;

        case "OTA_HotelAvail":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113");
            $requestIDDetails .= '
                <li> <b>101</b> : Successful request for multiple properties.</li>
                <li> <b>102</b> : Successful request for multiple properties with a Warning because one of the properties is invalid.</li>
                <li> <b>103</b> : Unsuccessful request for multiple properties with an error because the max number of 25 properties was exceeded.</li>
                <li> <b>104</b> : Unsuccessful request for a single property due to an invalid date.</li>
                <li> <b>105</b> : An unsuccessful multiple property request since one property is not available and the other property is invalid.</li>
                <li> <b>106</b> : Successful request for a single property that returns multiple rates.</li>
                <li> <b>107</b> : One of the properties has a rate change over stay. (RateIndicator="ChangeDuringStay") </li>
                <li> <b>108</b> : A single property request in which the actual rate changes over stay are returned in the response.</li>
                <li> <b>109</b> : Rates are requested in USD for a property in Tokyo.</li>
                <li> <b>110</b> : Only rooms that support an extra bed (rollaway or crib) are requested.  (RoomAmenity=”9”)</li>
                <li> <b>111</b> : Only rate plans associated with the clients ID number are returned in the response.</li>
                <li> <b>112</b> : Requesting for corporate rates with rate access codes.</li>
                <li> <b>113</b> : Requesting availability for a promotion code.  (PromotionCode=”P78WP”)</li>';

            //to display param fields
            $arrivalDate = ($_POST['arrival_date'])?$_POST['arrival_date']:'';
            $noOfNights = ($_POST['num_nights'])?$_POST['num_nights']:'';
            $noOfRooms = ($_POST['num_rooms'])?$_POST['num_rooms']:'';
            $noOfAdults = ($_POST['num_adults'])?$_POST['num_adults']:'';
            $noOfChilds = ($_POST['num_childs'])?$_POST['num_childs']:'';
            $roomAmenity = ($_POST['room_amenity'])?$_POST['room_amenity']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';
            $promotionCode = ($_POST['promotion_code'])?$_POST['promotion_code']:'';
            $currencyCode = ($_POST['currency_code'])?$_POST['currency_code']:'';
            $ratePlanCategory1 = ($_POST['rate_plan_category1'])?$_POST['rate_plan_category1']:'';
            $ratePlanID1 = ($_POST['rate_plan_id1'])?$_POST['rate_plan_id1']:'';
            $ratePlanCategory2 = ($_POST['rate_plan_category2'])?$_POST['rate_plan_category2']:'';
            $ratePlanID2 = ($_POST['rate_plan_id2'])?$_POST['rate_plan_id2']:'';
            $ratePlanCategory3 = ($_POST['rate_plan_category3'])?$_POST['rate_plan_category3']:'';
            $ratePlanID3 = ($_POST['rate_plan_id3'])?$_POST['rate_plan_id3']:'';

             //room amenity code list
             $roomAmenityCodeList = array ("0" => "One Bed", "1" => "Two Beds", "2" => "Suite", "3" => "Executive",
                                           "4" => "Towers", "5" => "Accessible", "6" => "Smoking", "7" => "Non-smoking",
                                           "8" => "Meal Plan", "9" => "Extra Bed", "10" => "Rollaway", "11" => "Crib");

            $paramArea.= '<tr>
                    <td class="label">Arrival Date* : </td>
                    <td><input type="text" name="arrival_date" id="arrival_date" value="'.$arrivalDate.'"/></td>
                    <td class="label">No of Nights* : </td>
                    <td><input type="text" name="num_nights" id="num_nights" value="'.$noOfNights.'"/></td>
                    <td class="label">No of Rooms* : </td>
                    <td><input type="text" name="num_rooms" id="num_rooms" value="'.$noOfRooms.'"/></td>
                   </tr>';
             $paramArea.= '<tr>
                    <td class="label">No of Adults* : </td>
                    <td><input type="text" name="num_adults" id="num_adults" value="'.$noOfAdults.'"/></td>
                    <td class="label">No of Childs : </td>
                    <td><input type="text" name="num_childs" id="num_childs" value="'.$noOfChilds.'"/></td>
                    <td class="label">Room Amenity : </td>';
                $paramArea.= '<td>
                        <select name="room_amenity" id="room_amenity" >
                            <option value="" >SELECT</option>';
                            foreach($roomAmenityCodeList as $key=>$value){
                                $roomAmentitySelected = ($roomAmenity==$key && $roomAmenity!=="")?"selected":'';
                                $paramArea.= "<option value='$key' $roomAmentitySelected >$value</option>";
                            }
             $paramArea.= '</select>
                    </td>
                </tr>';

             $paramArea.= '<tr>
                    <td class="label">Hotel Code* : </td>
                    <td colspan="5">
                        <input type="text" name="hotel_code" id="hotel_code" value="'.$hotelCode.'" style="width:810px;"/><br/>
                        [ <i>Please enter hotel codes seperated by comma (","). Eg : MEMGT,MEMPR </i> ]
                    </td>
                </tr>';

             $paramArea.= '<tr>
                    <td class="label">Currency Code : </td>
                    <td><input type="text" name="currency_code" id="currency_code" value="'.$currencyCode.'"/></td>
                    <td class="label">Promotion Code : </td>
                    <td><input type="text" name="promotion_code" id="promotion_code" value="'.$promotionCode.'"/></td>
                    <td colspan="2"></td>
                   </tr>';
             $paramArea.= '<tr>
                        <td class="label">Rate Plan Category1 : </td>
                        <td><input type="text" name="rate_plan_category1" id="rate_plan_category1" value="'.$ratePlanCategory1.'"/></td>
                        <td class="label">Rate Plan ID1 : </td>
                        <td><input type="text" name="rate_plan_id1" id="rate_plan_id1" value="'.$ratePlanID1.'"/></td>
                        <td class="label">Rate Plan Category2 : </td>
                        <td><input type="text" name="rate_plan_category2" id="rate_plan_category2" value="'.$ratePlanCategory2.'"/></td>
                    </tr>';
             $paramArea.= '<tr>
                        <td class="label">Rate Plan ID2 : </td>
                        <td><input type="text" name="rate_plan_id2" id="rate_plan_id2" value="'.$ratePlanID2.'"/></td>
                        <td class="label">Rate Plan Category3 : </td>
                        <td><input type="text" name="rate_plan_category3" id="rate_plan_category3" value="'.$ratePlanCategory3.'"/></td>
                        <td class="label">Rate Plan ID3 : </td>
                        <td><input type="text" name="rate_plan_id3" id="rate_plan_id3" value="'.$ratePlanID3.'"/></td>
                    </tr>';
            break;

        case "OTA_HotelRes":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103", "104", "105", "106", "107", "108", "109");
            $requestIDDetails .= '
                <li> <b>101</b> : Unsuccessful request because the specified room type is not available.</li>
                <li> <b>102</b> : Unsuccessful request because an invalid Credit Card number was specified.</li>
                <li> <b>103</b> : Successful request with ResStatus=”Initiate”.</li>
                <li> <b>104</b> : Unsuccessful request since the maximum occupancy is exceeded.</li>
                <li> <b>105</b> : Unsuccessful request since the guarantee is insufficient.</li>
                <li> <b>106</b> : Successful request with ResStatus=”Commit” and the reservation is guaranteed to Credit Card Deposit.</li>
                <li> <b>107</b> : Successful request with ResStatus=”Commit” and guaranteed to TA. (Note the rate changes in the response)</li>
                <li> <b>108</b> : Successful request for a corporate rate plan booking.</li>
                <li> <b>109</b> : Successful request for a Promotional rate plan.</li>';

            //to display param fields

            $arrivalDate = ($_POST['arrival_date'])?$_POST['arrival_date']:'';
            $noOfNights = ($_POST['num_nights'])?$_POST['num_nights']:'';
            $noOfAdults = ($_POST['num_adults'])?$_POST['num_adults']:'';
            $noOfChilds = ($_POST['num_childs'])?$_POST['num_childs']:'';
            $promotionCode = ($_POST['promotion_code'])?$_POST['promotion_code']:'';
            $roomTypeCode = ($_POST['room_type_code'])?$_POST['room_type_code']:'';
            $roomAmenity = ($_POST['room_amenity'])?$_POST['room_amenity']:'';
            $bookingCode = ($_POST['booking_code'])?$_POST['booking_code']:'';
            $ratePlanID = ($_POST['rate_plan_id'])?$_POST['rate_plan_id']:'';
            $ratePlanCode = ($_POST['rate_plan_code'])?$_POST['rate_plan_code']:'';
	    $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';

	    $namePrefix = ($_POST['name_prefix'])?$_POST['name_prefix']:'';
	    $customerName = ($_POST['customer_name'])?$_POST['customer_name']:'';
	    $surname = ($_POST['surname'])?$_POST['surname']:'';
	    $telephone = ($_POST['telephone'])?$_POST['telephone']:'';
	    $email = ($_POST['email'])?$_POST['email']:'';
	    $address1 = ($_POST['address1'])?$_POST['address1']:'';
	    $address2 = ($_POST['address2'])?$_POST['address2']:'';
	    $address3 = ($_POST['address3'])?$_POST['address3']:'';
	    $cityName = ($_POST['city_name'])?$_POST['city_name']:'';
	    $postalCode = ($_POST['postal_code'])?$_POST['postal_code']:'';
	    $stateCode = ($_POST['state_code'])?$_POST['state_code']:'';
	    $countryCode = ($_POST['country_code'])?$_POST['country_code']:'';

	    $comments1 = ($_POST['comments1'])?$_POST['comments1']:'';
	    $comments2 = ($_POST['comments2'])?$_POST['comments2']:'';
	    $comments3 = ($_POST['comments3'])?$_POST['comments3']:'';
            $memAccID1 = ($_POST['mem_acc_id1'])?$_POST['mem_acc_id1']:'';
            $memProgramCode1 = ($_POST['mem_program_code1'])?$_POST['mem_program_code1']:'';
            $memAccID2 = ($_POST['mem_acc_id2'])?$_POST['mem_acc_id2']:'';
            $memProgramCode2 = ($_POST['mem_program_code2'])?$_POST['mem_program_code2']:'';
            $guaranteeCode = ($_POST['guarantee_code'])?$_POST['guarantee_code']:'';
	    $cardCode = ($_POST['card_code'])?$_POST['card_code']:'';
	    $cardNumber = ($_POST['card_no'])?$_POST['card_no']:'';
	    $expireDate = ($_POST['expire_date'])?$_POST['expire_date']:'';
	    $resIDValue = ($_POST['res_id'])?$_POST['res_id']:'';
	    $resIDDate = ($_POST['res_date'])?$_POST['res_date']:'';
	    $resIDSource = ($_POST['res_source'])?$_POST['res_source']:'';
	    $resStatus = ($_POST['res_status'])?$_POST['res_status']:'';

            //brand code list
            $brandCodeList = array('HP' => 'Hampton Inns', 'HI' => 'Hilton Hotels', 'ES' => 'Embassy Suites', 'DT' => 'DoubleTree',
                                    'HW' => 'Homewood Suites', 'CH' => 'Conrad',  'GI' => 'Garden Inns', 'WA' => 'Waldorf=Astoria Collection');
            //booking code list
            $bookingCodeList = array('BAR' => 'Best Available Rate', 'CORPORATE' => 'Corporate Rate', 'CHAINWIDE' => 'Chainwide Rate',
                                    'LEISURE' => 'Leisure Rate', 'MEMBERSHIP' => 'Membership Rate', 'ADV PURCHASE' => 'Advance Purchase');
            //guarantee code list
             $guaranteeCodes = array('FP'=>'FULL PREPAYMENT NO CXL', 'CC'=>'CREDIT CARD GUARANTEE', 'TA'=>'GUARANTEE TO TRAVEL AGENT', 'CD'=>'TA GUARANTEE TO TRAVEL AGENT', 'CD'=>'CREDIT CARD DEPOSIT',
                                'CO'=>'GUARANTEE TO COMPANY', 'CR'=>'GUARANTEE TO COMPANY', '4P'=>'4PM HOLD', '6P'=>'6PM HOLD');
            //card code list
             $cardCodeList = array('AX' => 'American Express', '*BC' => 'Bank Card', '*BL' => 'Carte Bleu',' CB' => 'Carte Blanche',
                                'DN' => 'Diners Club', 'DS' => 'Discover Card',  '**EC' => 'Eurocard', 'JC' => 'Japanese Credit Bureau Credit Card',
                                'MC' => 'Master Card',  '*TP' => 'Universal Air Travel Card', 'VI' => 'Visa');

             //room amenity code list
             $roomAmenityCodeList = array ("0" => "One Bed", "1" => "Two Beds", "2" => "Suite", "3" => "Executive",
                                           "4" => "Towers", "5" => "Accessible", "6" => "Smoking", "7" => "Non-smoking",
                                           "8" => "Meal Plan", "9" => "Extra Bed", "10" => "Rollaway", "11" => "Crib");
             $paramArea.= '<tr>
                    <td class="label">Arrival Date* : </td>
                    <td><input type="text" name="arrival_date" id="arrival_date" value="'.$arrivalDate.'"/></td>
                    <td class="label">No of Nights* : </td>
                    <td><input type="text" name="num_nights" id="num_nights" value="'.$noOfNights.'"/></td>
                    <td class="label">No of Adults* : </td>
                    <td><input type="text" name="num_adults" id="num_adults" value="'.$noOfAdults.'"/></td>
                   </tr>';
             $paramArea.= '<tr>
                    <td class="label">No of Childs : </td>
                    <td><input type="text" name="num_childs" id="num_childs" value="'.$noOfChilds.'"/></td>
                    <td class="label">Promotion Code : </td>
                    <td><input type="text" name="promotion_code" id="promotion_code" value="'.$promotionCode.'"/></td>
                    <td class="label">Room Type Code* : </td>
                    <td><input type="text" name="room_type_code" id="room_type_code" value="'.$roomTypeCode.'"/></td>
                   </tr>';

             $paramArea.= '<tr>
                    <td class="label">Room Amenity : </td>';
                $paramArea.= '<td>
                        <select name="room_amenity" id="room_amenity" >
                            <option value="" >SELECT</option>';
                            foreach($roomAmenityCodeList as $key=>$value){
                                $roomAmentitySelected = ($roomAmenity==$key && $roomAmenity!=="")?"selected":'';
                                $paramArea.= "<option value='$key' $roomAmentitySelected >$value</option>";
                            }

             $paramArea.= '      </select>
                    </td>

                    <td class="label">Booking Code : </td>
                    <td>
                        <select name="booking_code" id="booking_code" >
                            <option value="" >SELECT</option>';
                            foreach($bookingCodeList as $key=>$value){
                                $bookingCodeSelected = ($bookingCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $bookingCodeSelected >$value</option>";
                            }

             $paramArea.= '      </select>
                    </td>
                    <td class="label">Rate Plan ID : </td>
                    <td><input type="text" name="rate_plan_id" id="rate_plan_id" value="'.$ratePlanID.'"/></td>
                   </tr>';

             $paramArea.= '<tr>
                    <td class="label">Rate Plan Code* : </td>
                    <td><input type="text" name="rate_plan_code" id="rate_plan_code" value="'.$ratePlanCode.'"/></td>
                    <td class="label">Brand Code* : </td>
                    <td>
                        <select name="brand_code" id="brand_code" >
                            <option value="" >SELECT</option>';
                            foreach($brandCodeList as $key=>$value){
                                $brandCodeSelected = ($brandCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $brandCodeSelected >$value</option>";
                            }
             $paramArea.= '      </select>
                    </td>
                    <td class="label">Hotel Code* : </td>
                    <td><input type="text" name="hotel_code" id="hotel_code" value="'.$hotelCode.'"/></td>
                   </tr>';

                  $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Customer Details </legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Name Prefix : </td>
                                    <td><input type="text" name="name_prefix" id="name_prefix" value="'.$namePrefix.'"/></td>
                                    <td class="label">Name* : </td>
                                    <td><input type="text" name="customer_name" id="customer_name" value="'.$customerName.'"/></td>
                                    <td class="label">Surname* : </td>
                                    <td><input type="text" name="surname" id="surname" value="'.$surname.'"/></td>
				  </tr>
                                 <tr>
                                    <td class="label">Telephone* : </td>
                                    <td><input type="text" name="telephone" id="telephone" value="'.$telephone.'"/></td>
                                    <td class="label">Email : </td>
                                    <td><input type="text" name="email" id="email" value="'.$email.'"/></td>
                                    <td class="label">Address1* : </td>
                                    <td><input type="text" name="address1" id="address1" value="'.$address1.'"/></td>
				  </tr>
                                 <tr>
                                    <td class="label">Address2 : </td>
                                    <td><input type="text" name="address2" id="address2" value="'.$address2.'"/></td>
                                    <td class="label">Address3 : </td>
                                    <td><input type="text" name="address3" id="address3" value="'.$address3.'"/></td>
                                    <td class="label">City Name* : </td>
                                    <td><input type="text" name="city_name" id="city_name" value="'.$cityName.'"/></td>
				  </tr>
                                 <tr>
                                    <td class="label">Postal Code : </td>
                                    <td><input type="text" name="postal_code" id="postal_code" value="'.$postalCode.'"/></td>
                                    <td class="label">State Code : </td>
                                    <td><input type="text" name="state_code" id="state_code" value="'.$stateCode.'"/></td>
                                    <td class="label">Country Code* : </td>
                                    <td><input type="text" name="country_code" id="country_code" value="'.$countryCode.'"/></td>
				  </tr>
                             </tbody></table></fieldset>
                       </tr>';

                  $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Membership Details </legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Account ID1 : </td>
                                    <td><input type="text" name="mem_acc_id1" id="mem_acc_id1" value="'.$memAccID1.'"/></td>
                                    <td class="label">Program Code1 : </td>
                                    <td><input type="text" name="mem_program_code1" id="mem_program_code1" value="'.$memProgramCode1.'"/></td>
                                 </tr>
                                 <tr>
                                    <td class="label">Account ID2 : </td>
                                    <td><input type="text" name="mem_acc_id2" id="mem_acc_id2" value="'.$memAccID2.'"/></td>
                                    <td class="label">Program Code2 : </td>
                                    <td><input type="text" name="mem_program_code2" id="mem_program_code2" value="'.$memProgramCode2.'"/></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
             $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Comments</legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Comments1 : </td>
                                    <td><input type="text" name="comments1" id="comments1" value="'.$comments1.'"/></td>
                                    <td class="label">Comments2 : </td>
                                    <td><input type="text" name="comments2" id="comments2" value="'.$comments2.'"/></td>
                                    <td class="label">Comments3 : </td>
                                    <td><input type="text" name="comments3" id="comments3" value="'.$comments3.'"/></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
             $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Guarantee & Card Details</legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Guarantee Code* : </td>
                                    <td colspan="2">';
             $paramArea.='<select name="guarantee_code" id="guarantee_code">';
                            foreach($guaranteeCodes as $key=>$value){

                                $gCodeSelected = ($guaranteeCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $gCodeSelected >$value</option>";
                            }//
                           $paramArea.= '</select></td><td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td class="label">Card Code : </td>
                                    <td>
                                        <select name="card_code" id="card_code">
                                            <option value="">SELECT</option>';
                            foreach($cardCodeList as $key=>$value){
                                $cardCodeSelected = ($cardCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $cardCodeSelected >$value</option>";
                            }//

                            $paramArea.= '</select></td>

                                    <td class="label">Card Number : </td>
                                    <td><input type="text" name="card_no" id="card_no" value="'.$cardNumber.'"/></td>
                                    <td class="label">Expire Date: </td>
                                    <td><input type="text" name="expire_date" id="expire_date" value="'.$expireDate.'"/></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
             $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Reservation Details</legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">ID : </td>
                                    <td><input type="text" name="res_id" id="res_id" value="'.$resIDValue.'"/></td>
                                    <td class="label">Date : </td>
                                    <td><input type="text" name="res_date" id="res_date" value="'.$resIDDate.'"/></td>
                                    <td class="label">Source : </td>
                                    <td><input type="text" name="res_source" id="res_source" value="'.$resIDSource.'"/></td>
                               </tr>
                               <tr>
                                    <td class="label">Status : </td>
                                    <td><input type="text" name="res_status" id="res_status" readOnly="true" value="'.$resStatus.'"/></td>
                                    <td colspan="4"></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
		break;
                
        case "OTA_HotelResModify":
            //to set request id and request ID details
            $reqIdList = array("101", "102", "103", "104");
            $requestIDDetails .= '
                <li> <b>101</b> : Unsuccessful request because of insufficient guarantee.</li>
                <li> <b>102</b> : Successful request with ResStatus=”Initiate”. The dura-tion, Rate Plan Code and the comments are modified.
                                    The total price changes because of the changes to duration and rate plan.
                                </li>
                <li> <b>103</b> : Same as above with ResStatus=”Commit”.</li>
                <li> <b>104</b> : Successful request. An email address is added and the comments are changed.</li>';

            //to display param fields

            $arrivalDate = ($_POST['arrival_date'])?$_POST['arrival_date']:'';
            $noOfNights = ($_POST['num_nights'])?$_POST['num_nights']:'';
            $noOfAdults = ($_POST['num_adults'])?$_POST['num_adults']:'';
            $noOfChilds = ($_POST['num_childs'])?$_POST['num_childs']:'';
            $promotionCode = ($_POST['promotion_code'])?$_POST['promotion_code']:'';
            $roomTypeCode = ($_POST['room_type_code'])?$_POST['room_type_code']:'';
            $roomAmenity = ($_POST['room_amenity'])?$_POST['room_amenity']:'';
            $bookingCode = ($_POST['booking_code'])?$_POST['booking_code']:'';
            $ratePlanID = ($_POST['rate_plan_id'])?$_POST['rate_plan_id']:'';
            $ratePlanCode = ($_POST['rate_plan_code'])?$_POST['rate_plan_code']:'';
	    $brandCode = ($_POST['brand_code'])?$_POST['brand_code']:'';
            $hotelCode = ($_POST['hotel_code'])?$_POST['hotel_code']:'';

	    $namePrefix = ($_POST['name_prefix'])?$_POST['name_prefix']:'';
	    $customerName = ($_POST['customer_name'])?$_POST['customer_name']:'';
	    $surname = ($_POST['surname'])?$_POST['surname']:'';
	    $telephone = ($_POST['telephone'])?$_POST['telephone']:'';
	    $email = ($_POST['email'])?$_POST['email']:'';
	    $address1 = ($_POST['address1'])?$_POST['address1']:'';
	    $address2 = ($_POST['address2'])?$_POST['address2']:'';
	    $address3 = ($_POST['address3'])?$_POST['address3']:'';
	    $cityName = ($_POST['city_name'])?$_POST['city_name']:'';
	    $postalCode = ($_POST['postal_code'])?$_POST['postal_code']:'';
	    $stateCode = ($_POST['state_code'])?$_POST['state_code']:'';
	    $countryCode = ($_POST['country_code'])?$_POST['country_code']:'';

	    $comments1 = ($_POST['comments1'])?$_POST['comments1']:'';
	    $comments2 = ($_POST['comments2'])?$_POST['comments2']:'';
	    $comments3 = ($_POST['comments3'])?$_POST['comments3']:'';
            $memAccID1 = ($_POST['mem_acc_id1'])?$_POST['mem_acc_id1']:'';
            $memProgramCode1 = ($_POST['mem_program_code1'])?$_POST['mem_program_code1']:'';
            $memAccID2 = ($_POST['mem_acc_id2'])?$_POST['mem_acc_id2']:'';
            $memProgramCode2 = ($_POST['mem_program_code2'])?$_POST['mem_program_code2']:'';
            $guaranteeCode = ($_POST['guarantee_code'])?$_POST['guarantee_code']:'';
	    $cardCode = ($_POST['card_code'])?$_POST['card_code']:'';
	    $cardNumber = ($_POST['card_no'])?$_POST['card_no']:'';
	    $expireDate = ($_POST['expire_date'])?$_POST['expire_date']:'';
	    $resIDValue = ($_POST['res_id'])?$_POST['res_id']:'';
	    $resIDDate = ($_POST['res_date'])?$_POST['res_date']:'';
	    $resIDSource = ($_POST['res_source'])?$_POST['res_source']:'';
	    $resStatus = ($_POST['res_status'])?$_POST['res_status']:'';
	    $localIdentificationNo = ($_POST['local_identification_no'])?$_POST['local_identification_no']:'';

            //brand code list
            $brandCodeList = array('HP' => 'Hampton Inns', 'HI' => 'Hilton Hotels', 'ES' => 'Embassy Suites', 'DT' => 'DoubleTree',
                                    'HW' => 'Homewood Suites', 'CH' => 'Conrad',  'GI' => 'Garden Inns', 'WA' => 'Waldorf=Astoria Collection');
            //booking code list
            $bookingCodeList = array('BAR' => 'Best Available Rate', 'CORPORATE' => 'Corporate Rate', 'CHAINWIDE' => 'Chainwide Rate',
                                    'LEISURE' => 'Leisure Rate', 'MEMBERSHIP' => 'Membership Rate', 'ADV PURCHASE' => 'Advance Purchase');
            //guarantee code list
             $guaranteeCodes = array('FP'=>'FULL PREPAYMENT NO CXL', 'CC'=>'CREDIT CARD GUARANTEE', 'TA'=>'GUARANTEE TO TRAVEL AGENT', 'CD'=>'TA GUARANTEE TO TRAVEL AGENT', 'CD'=>'CREDIT CARD DEPOSIT',
                                'CO'=>'GUARANTEE TO COMPANY', 'CR'=>'GUARANTEE TO COMPANY', '4P'=>'4PM HOLD', '6P'=>'6PM HOLD');
            //card code list
             $cardCodeList =array('AX' => 'American Express', '*BC' => 'Bank Card', '*BL' => 'Carte Bleu',' CB' => 'Carte Blanche',
                                'DN' => 'Diners Club', 'DS' => 'Discover Card',  '**EC' => 'Eurocard', 'JC' => 'Japanese Credit Bureau Credit Card',
                                'MC' => 'Master Card',  '*TP' => 'Universal Air Travel Card', 'VI' => 'Visa');
             //room amenity code list
             $roomAmenityCodeList = array ("0" => "One Bed", "1" => "Two Beds", "2" => "Suite", "3" => "Executive",
                                           "4" => "Towers", "5" => "Accessible", "6" => "Smoking", "7" => "Non-smoking",
                                           "8" => "Meal Plan", "9" => "Extra Bed", "10" => "Rollaway", "11" => "Crib");
             $paramArea.= '<tr>
                    <td class="label">Arrival Date* : </td>
                    <td><input type="text" name="arrival_date" id="arrival_date" value="'.$arrivalDate.'"/></td>
                    <td class="label">No of Nights* : </td>
                    <td><input type="text" name="num_nights" id="num_nights" value="'.$noOfNights.'"/></td>
                    <td class="label">No of Adults* : </td>
                    <td><input type="text" name="num_adults" id="num_adults" value="'.$noOfAdults.'"/></td>
                   </tr>';
             $paramArea.= '<tr>
                    <td class="label">No of Childs : </td>
                    <td><input type="text" name="num_childs" id="num_childs" value="'.$noOfChilds.'"/></td>
                    <td class="label">Promotion Code : </td>
                    <td><input type="text" name="promotion_code" id="promotion_code" value="'.$promotionCode.'"/></td>
                    <td class="label">Room Type Code* : </td>
                    <td><input type="text" name="room_type_code" id="room_type_code" value="'.$roomTypeCode.'"/></td>
                   </tr>';

             $paramArea.= '<tr>
                    <td class="label">Room Amenity : </td>';
                $paramArea.= '<td>
                        <select name="room_amenity" id="room_amenity" >
                            <option value="" >SELECT</option>';
                            foreach($roomAmenityCodeList as $key=>$value){
                                $roomAmentitySelected = ($roomAmenity==$key && $roomAmenity!=="")?"selected":'';
                                $paramArea.= "<option value='$key' $roomAmentitySelected >$value</option>";
                            }

             $paramArea.= '      </select>
                    </td>
                    <td class="label">Booking Code : </td>
                    <td>
                        <select name="booking_code" id="booking_code" >
                            <option value="" >SELECT</option>';
                            foreach($bookingCodeList as $key=>$value){
                                $bookingCodeSelected = ($bookingCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $bookingCodeSelected >$value</option>";
                            }

             $paramArea.= '      </select>
                    </td>
                    <td class="label">Rate Plan ID : </td>
                    <td><input type="text" name="rate_plan_id" id="rate_plan_id" value="'.$ratePlanID.'"/></td>
                   </tr>';

             $paramArea.= '<tr>
                    <td class="label">Rate Plan Code* : </td>
                    <td><input type="text" name="rate_plan_code" id="rate_plan_code" value="'.$ratePlanCode.'"/></td>
                    <td class="label">Brand Code* : </td>
                    <td>
                        <select name="brand_code" id="brand_code" >
                            <option value="" >SELECT</option>';
                            foreach($brandCodeList as $key=>$value){
                                $brandCodeSelected = ($brandCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $brandCodeSelected >$value</option>";
                            }
             $paramArea.= '      </select>
                    </td>
                    <td class="label">Hotel Code* : </td>
                    <td><input type="text" name="hotel_code" id="hotel_code" value="'.$hotelCode.'"/></td>
                   </tr>';

                  $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Customer Details </legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Name Prefix : </td>
                                    <td><input type="text" name="name_prefix" id="name_prefix" value="'.$namePrefix.'"/></td>
                                    <td class="label">Name* : </td>
                                    <td><input type="text" name="customer_name" id="customer_name" value="'.$customerName.'"/></td>
                                    <td class="label">Surname* : </td>
                                    <td><input type="text" name="surname" id="surname" value="'.$surname.'"/></td>
				  </tr>
                                 <tr>
                                    <td class="label">Telephone* : </td>
                                    <td><input type="text" name="telephone" id="telephone" value="'.$telephone.'"/></td>
                                    <td class="label">Email : </td>
                                    <td><input type="text" name="email" id="email" value="'.$email.'"/></td>
                                    <td class="label">Address1* : </td>
                                    <td><input type="text" name="address1" id="address1" value="'.$address1.'"/></td>
				  </tr>
                                 <tr>
                                    <td class="label">Address2 : </td>
                                    <td><input type="text" name="address2" id="address2" value="'.$address2.'"/></td>
                                    <td class="label">Address3 : </td>
                                    <td><input type="text" name="address3" id="address3" value="'.$address3.'"/></td>
                                    <td class="label">City Name* : </td>
                                    <td><input type="text" name="city_name" id="city_name" value="'.$cityName.'"/></td>
				  </tr>
                                 <tr>
                                    <td class="label">Postal Code : </td>
                                    <td><input type="text" name="postal_code" id="postal_code" value="'.$postalCode.'"/></td>
                                    <td class="label">State Code : </td>
                                    <td><input type="text" name="state_code" id="state_code" value="'.$stateCode.'"/></td>
                                    <td class="label">Country Code* : </td>
                                    <td><input type="text" name="country_code" id="country_code" value="'.$countryCode.'"/></td>
				  </tr>
                             </tbody></table></fieldset>
                       </tr>';

                  $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Membership Details </legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Account ID1 : </td>
                                    <td><input type="text" name="mem_acc_id1" id="mem_acc_id1" value="'.$memAccID1.'"/></td>
                                    <td class="label">Program Code1 : </td>
                                    <td><input type="text" name="mem_program_code1" id="mem_program_code1" value="'.$memProgramCode1.'"/></td>
                                 </tr>
                                 <tr>
                                    <td class="label">Account ID2 : </td>
                                    <td><input type="text" name="mem_acc_id2" id="mem_acc_id2" value="'.$memAccID2.'"/></td>
                                    <td class="label">Program Code2 : </td>
                                    <td><input type="text" name="mem_program_code2" id="mem_program_code2" value="'.$memProgramCode2.'"/></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
             $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Comments</legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Comments1 : </td>
                                    <td><input type="text" name="comments1" id="comments1" value="'.$comments1.'"/></td>
                                    <td class="label">Comments2 : </td>
                                    <td><input type="text" name="comments2" id="comments2" value="'.$comments2.'"/></td>
                                    <td class="label">Comments3 : </td>
                                    <td><input type="text" name="comments3" id="comments3" value="'.$comments3.'"/></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
             $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Guarantee & Card Details</legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">Guarantee Code* : </td>
                                    <td colspan="2">';
             $paramArea.='<select name="guarantee_code" id="guarantee_code">';
                            foreach($guaranteeCodes as $key=>$value){

                                $gCodeSelected = ($guaranteeCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $gCodeSelected >$value</option>";
                            }//
                           $paramArea.= '</select></td><td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td class="label">Card Code : </td>
                                    <td>
                                        <select name="card_code" id="card_code">
                                            <option value="">SELECT</option>';
                            foreach($cardCodeList as $key=>$value){
                                $cardCodeSelected = ($cardCode==$key)?"selected":'';
                                $paramArea.= "<option value='$key' $cardCodeSelected >$value</option>";
                            }//

                            $paramArea.= '</select></td>

                                    <td class="label">Card Number : </td>
                                    <td><input type="text" name="card_no" id="card_no" value="'.$cardNumber.'"/></td>
                                    <td class="label">Expire Date: </td>
                                    <td><input type="text" name="expire_date" id="expire_date" value="'.$expireDate.'"/></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
             $paramArea.= '<tr>
                    <td colspan="6">
                        <fieldset><legend>Reservation Details</legend>
                            <table class="param_inner_table"><tbody>
                                <tr>
                                    <td class="label">ID : </td>
                                    <td><input type="text" name="res_id" id="res_id" value="'.$resIDValue.'"/></td>
                                    <td class="label">Date : </td>
                                    <td><input type="text" name="res_date" id="res_date" value="'.$resIDDate.'"/></td>
                                    <td class="label">Source : </td>
                                    <td><input type="text" name="res_source" id="res_source" value="'.$resIDSource.'"/></td>
                               </tr>
                               <tr>
                                    <td class="label">Local Identification No : </td>
                                    <td><input type="text" name="local_identification_no" id="local_identification_no"  value="'.$localIdentificationNo.'"/></td>
                                    <td class="label">Status : </td>
                                    <td><input type="text" name="res_status" id="res_status" readOnly="true" value="'.$resStatus.'"/></td>
                                    <td colspan="2"></td>
                               </tr>
                            </tbody></table></fieldset>
                       </tr>';
		break;

        default:
            break;
    }//switch
    $paramArea.= '</tbody></table></fieldset>';
    $reqIdArea = "<div class='request_id_area'>
                    <fieldset>
                        <legend>Request ID Details</legend>
                        <div class='request_id_field'>
                            Request ID* :&nbsp;&nbsp;
                            <select name='request_id' id='request_id'>";
            foreach($reqIdList as $reqID){
                $reqSelected = ($reqID==$_POST['request_id'])?"selected":"";
                $reqIdArea .= "<option value='$reqID' $reqSelected >$reqID</option>";
            }
        $reqIdArea .= " </select>
                        </div>
                        <div>
                            <ul>
                                $requestIDDetails
                            </ul>
                        </div>
                    </fieldset>
            </div>";
        
    //to set the outut
    $output = $reqIdArea.$paramArea;
    
    return $output;
}//function

//service names list should be added here
$serviceNames = array('OTA_Ping', 'OTA_HotelSearch', 'OTA_HotelAvail', 'OTA_HotelRes', 'OTA_HotelResModify', 'OTA_Read', 'OTA_Cancel', 'OTA_HotelDescriptiveInfo');
?>
<html>
    <head>
        <title>HTE Direct Connect</title>
        <script type="text/javascript" src="jquery-1.7.1.min.js" ></script>
        <script type="text/javascript" src="hte.js" ></script>

        <style type="text/css">
            .button{cursor:pointer;font-weight:bold;color:#FFFFFF;background-color:#808080;}
            .raw_resp{width:500px;height:300px;}
            .xml_resp{overflow:auto;border:1px solid #808080;}
            .param_inner_table{width:99%}
            .hilton_form_tbl{width:100%;}
             legend{font-weight:bold;}
             td.label{padding-left: 20px;padding-right: 10px; text-align:right;width:150px;}
            .loading{display:none; padding:20px;}
            .error {color:#FF0000; border:1px solid #808080;padding:40px;}
            .request_id_field{text-align:center;}
            .container {text-align:center;}
        </style>

    </head>
    <body>
        <div class="container">
           <h2>HTE Direct Connect</h2>

            <div class="select_service_area">
                <fieldset>
                    <form method="POST" name="hiltonOTAForm" action="" onsubmit='return validateHTEForm();'>
                        <table cellpadding="5" cellspacing="5" class="hilton_form_tbl">
                            <tbody>
                                <tr>
                                    <td style='text-align:center'>Service Name* :&nbsp;&nbsp;
                                        <select name="service_name" id="service_name" onchange="getRequestParameterArea();">
                                            <option value="" >SELECT</option>
                                            <?php
                                                foreach($serviceNames as $service){
                                                    $selected = ($service==$_POST['service_name'])?"selected":'';
                                                    echo "<option value='$service' $selected>$service</option>";
                                                }
                                            ?>
                                        </select>
                                        <div class="loading" id="loading"><img src="loader.gif" alt="Loading"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="req_param_area" class="req_param_area">
                                            <?php if($_POST['submitOTAForm']=='Y'){
                                                //to display the parameters area
                                                echo getParameterArea ($_POST['service_name']);
                                            } ?>
                                        </div>

                                    </td>

                                </tr>
                                <tr>
                                    <td style='text-align:center'>
                                        <input type="submit" name="submit" value="SEND" class="button"/>
                                        <input type="hidden" name="submitOTAForm" value="Y" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </fieldset>
            </div>
            <?php if($_POST['submitOTAForm']=='Y'){ ?>
                <div class="display_results_area" id="display_results_area">
                    <?php callHiltonService($_POST['service_name']);//to display the hilton service results ?>
                </div>
            <?php } ?>

        </div>
        <?php
        if($_POST['submitOTAForm']=='Y'){?>
            <script language="javascript" type="text/javascript">$('#request_id').bind({change : setDefaultParameters});</script>
        <?php }?>
    </body>
</html>


